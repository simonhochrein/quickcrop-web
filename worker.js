importScripts("libs.js");

self.addEventListener('message', function (event) {
    var data = event.data;
    var mat = new Mat(2, data.mat._width, data.mat._height, data.mat._buffer);
    var padding = 20;
    var doublePadding = padding * 2;
    var crop = Crop.getCrop(mat);
    var ratio = (data.width / 150);
    var ret = { x: crop.x * ratio - padding, y: crop.y * ratio - padding, width: crop.width * ratio + doublePadding, height: crop.height * ratio + doublePadding };
    event.ports[0].postMessage(ret);
});