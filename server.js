var express = require('express');

var server = express();
server.use((req, res, next) => {
    console.log(req.connection.remoteAddress, req.url);
    next();
});
server.use(express.static(__dirname));

server.listen(80);