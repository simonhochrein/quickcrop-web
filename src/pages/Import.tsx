import { Grid, Icon, Image as S_Image, Header, Portal, Segment, Button, Menu } from "semantic-ui-react";
import * as React from "react";
import { PageProps, PageMenuProps } from './Page';
import { Settings } from "../components/Settings";

export class Import extends React.Component<PageProps, { files: string[], cropped: number[] }> {
    constructor(props: PageProps) {
        super(props);
        this.state = {
            files: [],
            cropped: []
        }
    }
    render() {
        return (
            <Grid style={{ overflow: "auto" }} verticalAlign={this.state.files.length == 0 ? "middle" : "top"} centered={this.state.files.length == 0} className="flex-1" padded={"horizontally"}>
                {this.state.files.length == 0 ?
                    <Grid.Column textAlign="center">
                        <Header inverted size={"huge"}>
                            Drop Files Or&nbsp;
                            <a href="#" onClick={this.props.openFilePicker.bind(this)}>Select</a>
                        </Header>
                    </Grid.Column>
                    :
                    <Grid.Row style={{ minHeight: 'min-content' }}>
                        {this.state.files.map((file, key) =>
                            <Grid.Column width={4} key={key} className="extraPadding">
                                <S_Image src={file} className={"image-element"} />
                            </Grid.Column>
                        )}
                    </Grid.Row>
                }
            </Grid>
        );
    }
    componentWillReceiveProps({ files, cropped }: PageProps) {
        this.setState({ files, cropped });
    }
}

export class ImportMenu extends React.Component<PageMenuProps, { settingsOpen: boolean }> {
    constructor(props: PageMenuProps) {
        super(props);
        this.state = {
            settingsOpen: false
        };
    }
    render() {
        return (
            <Menu icon='labeled'>
                <Settings open={this.state.settingsOpen} close={() => { this.setState({ settingsOpen: false }) }} />
                <Menu.Item name='settings' onClick={() => { this.setState({ settingsOpen: true }) }}>
                    <Icon name='settings' />
                    Settings
                </Menu.Item>
                <Menu.Item name='arrow right' position="right" onClick={this.props.next}>
                    <Icon name='arrow right' />
                    Next
                </Menu.Item>
            </Menu>
        );
    }
}