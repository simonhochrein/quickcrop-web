export interface PageProps {
    files: string[];
    stitched: string[];
    cropped: number[];
    toggleCrop(key: number): void;
    unStitch(key: number): void;
    openFilePicker(): void;
    stitch(keyOne: number, keyTwo: number): void;
}

export interface PageMenuProps {
    next(): void;
    cropAll(): void;
    reset(): void;
    settingsUpdate(): void;
    export(): void;
}