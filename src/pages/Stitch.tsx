import * as React from 'react';
import { PageProps, PageMenuProps } from './Page';
import { Grid, Image as S_Image, Menu, Icon } from 'semantic-ui-react';

export class Stitch extends React.Component<PageProps, { files: string[], stitched: string[], cropped: number[], selected: number }> {
    constructor(props: PageProps) {
        super(props);
        this.state = {
            files: [],
            stitched: [],
            cropped: [],
            selected: -1
        }
    }
    render() {
        return (
            <Grid className="flex-1" padded={"horizontally"} columns="equal">
                <Grid.Column style={{ overflow: "auto" }}>
                    <Grid style={{ minHeight: 'min-content' }} >
                        {this.state.files.map((file, key) =>
                            <Grid.Column width={4} key={key} className="extraPadding">
                                <div style={{ border: 'solid 4px', borderColor: this.state.selected == key ? '#E37534' : 'transparent' }}>
                                    <S_Image src={file} className={"image-element"} onClick={this.toggleSelect.bind(this, key)} />
                                </div>
                            </Grid.Column>
                        )}
                    </Grid>
                </Grid.Column>
                <Grid.Column style={{ overflow: "auto", backgroundColor: "#555555" }}>
                    <Grid style={{ minHeight: 'min-content' }} >
                        {this.state.stitched.map((file, key) =>
                            <Grid.Column width={8} key={key} className="extraPadding">
                                <S_Image src={file} className={"image-element"} onClick={() => { this.props.unStitch(key) }} />
                            </Grid.Column>
                        )}
                    </Grid>
                </Grid.Column>
            </Grid>
        );
    }
    toggleSelect(key) {
        var selected = this.state.selected;
        if (key === selected) {
            this.setState({ selected: -1 });
        } else if (selected !== -1) {
            this.setState({ selected: -1 });
            this.props.stitch(selected, key);
        } else {
            selected = key;
            this.setState({ selected });
        }
    }
    componentDidMount() {
        var { files, cropped } = this.props;
        this.setState({ files, cropped });
    }
    componentWillReceiveProps({ files, cropped, stitched }: PageProps) {
        this.setState({ files, cropped, stitched });
    }
}

export class StitchMenu extends React.Component<PageMenuProps, any> {
    render() {
        return (
            <Menu icon='labeled'>
                {/* <Menu.Item name='magic' onClick={this.props.cropAll}>
                    <Icon name='magic' />
                    Auto Stitch
                </Menu.Item> */}

                <Menu.Item name='arrow right' position="right" onClick={this.props.export}>
                    <Icon name='download' />
                    Download
                </Menu.Item>
            </Menu>
        );
    }
}