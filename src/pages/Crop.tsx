import { Grid, Icon, Image as S_Image, Portal, Button, Menu } from "semantic-ui-react";
import * as React from "react";
import { PageProps, PageMenuProps } from "./Page";
import { Settings } from "../components/Settings";

export class Crop extends React.Component<PageProps, { files: string[], cropped: number[] }> {
    constructor(props: PageProps) {
        super(props);
        this.state = {
            files: [],
            cropped: []
        }
    }
    render() {
        return (
            <Grid style={{ overflow: "auto" }} padded={"horizontally"} className="flex-1">
                <Grid.Row style={{ minHeight: 'min-content' }}>
                    {this.state.files.map((file, key) =>
                        <Grid.Column width={4} key={key} className="extraPadding">
                            <div className="image-dimmer-container" onClick={this.props.toggleCrop.bind(this, key)}>
                                <S_Image src={file} className={"image-element"} />
                                <div className="ui dimmer transition image-dimmer">
                                    <Icon name={this.props.cropped.indexOf(key) > -1 ? "repeat" : "crop"} size="huge" inverted />
                                </div>
                            </div>
                        </Grid.Column>
                    )}
                </Grid.Row>
            </Grid>
        );
    }
    componentDidMount() {
        var { files, cropped } = this.props;
        this.setState({ files, cropped });
    }
    componentWillReceiveProps({ files, cropped }: PageProps) {
        this.setState({ files, cropped });
    }
}

export class CropMenu extends React.Component<PageMenuProps, { settingsOpen: boolean }> {
    constructor(props: PageMenuProps) {
        super(props);
        this.state = {
            settingsOpen: false
        };
    }
    render() {
        return (
            <Menu icon='labeled'>
                <Settings open={this.state.settingsOpen} close={() => { this.setState({ settingsOpen: false }) }} onUpdated={this.props.settingsUpdate} />
                <Menu.Item name='settings' onClick={() => { this.setState({ settingsOpen: true }) }}>
                    <Icon name='settings' />
                    Settings
                </Menu.Item>
                <Menu.Item name='repeat' onClick={this.props.reset}>
                    <Icon name='repeat' />
                    Reset
                </Menu.Item>

                <Menu.Item name='magic' onClick={this.props.cropAll}>
                    <Icon name='magic' />
                    Crop All
                </Menu.Item>

                <Menu.Item name='arrow right' position="right" onClick={this.props.next}>
                    <Icon name='arrow right' />
                    Next
                </Menu.Item>
            </Menu>
        );
    }
}