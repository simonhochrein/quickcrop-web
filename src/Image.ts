import Mat from './Mat';

var createCanvas = function (width, height) {
    var canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    return canvas.getContext('2d');
}

export default class Image {
    public static loadIMG(image: HTMLImageElement, cb: (orig: Mat, mat: Mat, ratio: number) => void) {
        var width = 150;
        var height = (image.height / image.width) * width;
        var c1 = createCanvas(image.width, image.height);
        c1.drawImage(image, 0, 0);
        var im = c1.getImageData(0, 0, image.width, image.height);

        var c2 = createCanvas(width, height);
        c2.drawImage(image, 0, 0, width, height);
        var im2 = c2.getImageData(0, 0, width, height);
        cb(new Mat(Mat.types.RGBA, image.width, image.height, im.data), new Mat(Mat.types.RGBA, width, height, im2.data), (image.height / image.width));
    }
}