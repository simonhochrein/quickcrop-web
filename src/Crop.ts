import Mat from "./Mat";
import Color from "./Color";

export default class Crop {
    static Threshold = 60;
    static getCrop(originalMat: Mat) {
        let mat = originalMat.clone();
        // mat.toGrayscale();
        var avgRed = 0;
        var avgBlue = 0;
        var avgGreen = 0;
        // for (var y = 0; y < 100; y++) {
        //     var p = mat.pixel(10, y);
        //     avgRed += p.red;
        //     avgBlue += p.blue;
        //     avgGreen += p.green;
        // }
        // var avgColor = new Color(avgRed / 100, avgGreen / 100, avgBlue / 100);
        var firstX = mat.width;
        var lastX = 0;
        var firstY = mat.height;
        var lastY = 0;
        var offset = 0;

        var points: { x: number, y: number }[] = [];

        for (var y = offset; y < mat.height - offset; y++) {
            for (var x = offset; x < mat.width - offset; x++) {
                var xys = Crop.getCircle(x, y);
                var sample = mat.pixel(x, y);
                // if (Crop.compare(sample, avgColor)) {
                // var truths = 0;
                for (var i = 0; i < xys.length; i++) {
                    var sample2 = mat.pixel(xys[i][0], xys[i][1]);
                    if (Crop.compare(sample, sample2)) {
                        points.push({ x, y });
                        continue;
                    }
                }
                // }
            }
        }

        var distance = 2;
        var minSize = 8;

        for (var i = 0; i < points.length; i++) {
            if (points.filter((point) => {
                return Math.abs(points[i].x - point.x) < distance && Math.abs(points[i].y - point.y) < distance
            }).length > minSize) {
                if (points[i].x < firstX) {
                    firstX = points[i].x;
                }
                if (points[i].x > lastX) {
                    lastX = points[i].x;
                }
                if (points[i].y < firstY) {
                    firstY = points[i].y;
                }
                if (points[i].y > lastY) {
                    lastY = points[i].y;
                }
            }
        }
        return {
            x: firstX, y: firstY, width: (lastX - firstX), height: (lastY - firstY)
        }
    }
    static compare(color1: Color, color2: Color) {
        if (Math.abs(color1.red - color2.red) > Crop.Threshold
            && Math.abs(color1.green - color2.green) > Crop.Threshold
            && Math.abs(color1.blue - color2.blue) > Crop.Threshold) {
            return true;
        }
        return false;
    }
    static getCircle(x, y): [number, number][] {
        return [
            [x, y - 3],
            // [x, y - 2],
            // [x, y - 1],

            [x + 2, y - 2],
            // [x + 1, y - 1],

            [x + 3, y],
            // [x + 2, y],
            // [x + 1, y],

            [x + 2, y + 2],
            // [x + 1, y + 1],

            [x, y + 3],
            // [x, y + 2],
            // [x, y + 1],

            [x - 2, y + 2],
            // [x - 1, y + 1],

            [x - 3, y],
            // [x - 2, y],
            // [x - 1, y],

            [x - 2, y - 2],
            // [x - 1, y - 1]
        ]
    }
}