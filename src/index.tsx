var worker = new Worker('worker2.js');

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Button, Grid, List, Segment, Icon, Image as S_Image, Dimmer, Loader, Step } from 'semantic-ui-react';
import Header from 'semantic-ui-react/dist/commonjs/elements/Header/Header';
var JSZip = require('jszip');

require('../css/index');

import async, { select } from 'async';
import Container from 'semantic-ui-react/dist/commonjs/elements/Container/Container';
import { ChangeEvent } from 'react';
import { Import, ImportMenu } from './pages/Import';
import { Crop, CropMenu } from './pages/Crop';
import { Stitch, StitchMenu } from './pages/Stitch';

if (!localStorage.getItem("width")) {
    localStorage.setItem("width", "400");
    localStorage.setItem("padding", "20");
}

var steps = [
    { content: Import, menu: ImportMenu },
    { content: Crop, menu: CropMenu },
    { content: Stitch, menu: StitchMenu }
];

class App extends React.Component {
    fileInput: HTMLInputElement = null;
    state = {
        files: [],
        images: [],
        stitched: [],
        loading: false,
        currentlyLoading: 0,
        maxLoading: 0,
        cropped: [],
        step: 0
    };

    render() {
        var Content = steps[this.state.step].content;
        var Menu = steps[this.state.step].menu;
        return (
            <div className={"flex-1 display-flex"} onDrop={this.onDrop.bind(this)} onDragOver={this.onOver.bind(this)}>
                <input type="file" multiple style={{ display: "none" }} ref={(e) => { this.fileInput = e; }} onChange={this.onChange.bind(this)} />
                <Grid className="main-content">
                    <Dimmer active={this.state.loading} className="main-dimmer">
                        <Loader size='huge'>
                            Processing {this.state.currentlyLoading} of {this.state.maxLoading}<br />
                            Please Wait
                        </Loader>
                    </Dimmer>
                    <Grid.Row>
                        <Grid.Column className="display-flex-column">
                            <Step.Group>
                                <Step active={this.state.step == 0}>
                                    <Icon name='upload' />
                                    <Step.Content>
                                        <Step.Title>Import</Step.Title>
                                    </Step.Content>
                                </Step>

                                <Step active={this.state.step == 1} disabled={this.state.step < 1}>
                                    <Icon name='crop' />
                                    <Step.Content>
                                        <Step.Title>Crop</Step.Title>
                                    </Step.Content>
                                </Step>

                                <Step active={this.state.step == 2} disabled={this.state.step < 2}>
                                    <Icon name='exchange' />
                                    <Step.Content>
                                        <Step.Title>Combine</Step.Title>
                                    </Step.Content>
                                </Step>
                            </Step.Group>
                            <Content
                                cropped={this.state.cropped}
                                files={this.state.files}
                                toggleCrop={this.toggleCrop.bind(this)}
                                openFilePicker={this.openFilePicker.bind(this)}
                                stitch={this.stitch.bind(this)}
                                stitched={this.state.stitched}
                                unStitch={this.unStitch.bind(this)}
                            />
                            <Menu
                                next={this.next.bind(this)}
                                cropAll={this.cropAll.bind(this)}
                                reset={this.reset.bind(this)}
                                settingsUpdate={this.settingsUpdate.bind(this)}
                                export={this.export.bind(this)}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
    settingsUpdate() {
        if (this.state.cropped.length > 0) {
            this.setState({ loading: true, currentlyLoading: 1, maxLoading: this.state.cropped.length });
            this.state.cropped.forEach((key) => {
                this.crop(this.state.images[key], key);
            });
        }
    }
    toggleCrop(key: number) {
        var index = this.state.cropped.indexOf(key);
        if (index > -1) {
            var cropped = this.state.cropped;
            cropped.splice(index, 1);
            var files = this.state.files;
            files[key] = this.state.images[key].src;
            this.setState({ files, cropped });
        } else {
            this.cropOne(key);
        }
    }
    reset() {
        var files = this.state.files;
        files.forEach((file, key) => {
            files[key] = this.state.images[key].src;
        });
        this.setState({ files, cropped: [] });
    }
    next() {
        this.setState({ step: this.state.step + 1 });
    }
    openFilePicker() {
        this.fileInput.click();
    }
    onOver(e: DragEvent) {
        e.preventDefault();
        e.dataTransfer.effectAllowed = "all";
    }
    readFiles(files: FileList) {
        var i = 0;
        files = [].slice.call(files).sort((file1: File, file2: File) => {
            var number1 = /[0-9]+/g.exec(file1.name)[0];
            var number2 = /[0-9]+/g.exec(file2.name)[0];
            return number1 > number2;
        });
        var end = files.length;
        async.forEach(files, (value, callback) => {
            var fr = new FileReader();
            fr.onload = () => {
                var files = this.state.files;
                var images = this.state.images;
                var img = document.createElement('img');
                img.src = fr.result;
                images.push(img);
                files.push(fr.result);
                img.onload = () => {
                    if (++i == end) {
                        this.setState({ loading: false, files });
                    } else {
                        this.setState({ currentlyLoading: i, files });
                    }
                    callback();
                }
            }
            fr.readAsDataURL(value);
        });
    }
    onDrop(e: DragEvent) {
        e.preventDefault();
        this.readFiles(e.dataTransfer.files);
        this.setState({ loading: true, currentlyLoading: 1, maxLoading: e.dataTransfer.files.length });
    }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        this.readFiles(e.target.files);
        this.setState({ loading: true, currentlyLoading: 1, maxLoading: e.target.files.length });
    }
    export() {
        var zip = new JSZip();
        var images = zip.folder("images");
        var combined = zip.folder("combined");
        this.setState({ loading: true, maxLoading: this.state.files.length + this.state.stitched.length, currentlyLoading: 0 });
        var i = 0;
        this.state.files.forEach((img, key) => {
            images.file(key + ".png", img.slice("data:image/png;base64,".length, img.length), { base64: true });
            this.setState({ currentlyLoading: ++i });
        });
        this.state.stitched.forEach((img, key) => {
            combined.file(key + ".png", img.slice("data:image/png;base64,".length, img.length), { base64: true });
            this.setState({ currentlyLoading: ++i });
        });
        zip.generateAsync({ type: "blob" }).then((blob) => {
            var d = new Date();
            this.setState({ loading: false });
            var a = document.createElement("a");
            a.href = window.URL.createObjectURL(blob);
            a.download = `${d.getMonth() + 1}-${d.getDate()}-${d.getFullYear()}-export.zip`;
            a.click();
            location.reload();
        }, function (err) {
            console.log(err);
        });
    }
    componentDidMount() {
        worker.onmessage = (e) => {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var { height, width, x, y, i } = e.data;
            canvas.height = height;
            canvas.width = width;
            ctx.drawImage(this.state.images[i], -x, -y);
            var files = this.state.files;
            files[i] = canvas.toDataURL("png");
            if (this.state.currentlyLoading == this.state.maxLoading) {
                this.setState({ files, loading: false });
            } else {
                this.setState({ files, currentlyLoading: this.state.currentlyLoading + 1 });
            }
            var cropped = this.state.cropped;
            cropped.push(i);
            this.setState({ cropped });
        }
    }
    crop(img, i) {
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        var data = ctx.getImageData(0, 0, img.width, img.height).data;
        worker.postMessage({ width: img.width, height: img.height, data: data, i, maxWidth: localStorage.getItem('width'), padding: localStorage.getItem('padding') });
    }
    unStitch(key) {
        var stitched = this.state.stitched;
        stitched.splice(key, 1);
        this.setState({ stitched });
    }
    stitch(key, key2) {
        var canvas = document.createElement('canvas');
        var maxWidth = parseInt(localStorage.getItem("width"));

        var ctx = canvas.getContext('2d');
        var img = new Image();
        var img2 = new Image();
        img.onload = () => {
            img2.onload = () => {
                if (img.height < img2.height) {
                    var height = (img.height / img.width) * maxWidth;
                    var width = (img2.width / img2.height) * height;
                    canvas.width = maxWidth + width;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, maxWidth, height);
                    ctx.drawImage(img2, maxWidth, 0, width, height);
                } else if (img.height > img2.height) {
                    var height = (img2.height / img2.width) * maxWidth;
                    var width = (img.width / img.height) * height;
                    canvas.width = maxWidth + width;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, maxWidth, height);
                    ctx.drawImage(img2, maxWidth, 0, width, height);
                } else {
                    var height = (img.height / img.width) * maxWidth;
                    canvas.width = maxWidth + maxWidth;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, maxWidth, height);
                    ctx.drawImage(img2, maxWidth, 0, maxWidth, height);
                }
                var stitched = this.state.stitched;
                stitched.push(canvas.toDataURL("png"));
                this.setState({ stitched });
            }
            img2.src = this.state.files[key2];
        }
        img.src = this.state.files[key];
    }
    cropOne(index) {
        this.setState({ loading: true, currentlyLoading: 1, maxLoading: 1 });
        this.crop(this.state.images[index], index);
    }
    cropAll() {
        let files = this.state.files;
        if (this.state.cropped.length < files.length) {
            this.setState({ loading: true, currentlyLoading: 1, maxLoading: files.length - this.state.cropped.length }, () => {
                this.state.images.forEach((img, key) => {
                    if (this.state.cropped.indexOf(key) == -1) {
                        this.crop(img, key);
                    }
                });
            });
        }
    }
}

ReactDOM.render(<App />, document.getElementById('root'));