import * as React from 'react';
import { Menu, Modal, Button, Icon, Header, Form } from 'semantic-ui-react';
import FormInput from 'semantic-ui-react/dist/commonjs/collections/Form/FormInput';

export class Settings extends React.Component<{ open: boolean, close(), onUpdated?() }, { open: boolean, padding: number, width: number }> {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            padding: parseInt(localStorage.getItem("padding")),
            width: parseInt(localStorage.getItem("width"))
        };
    }
    render() {
        return (
            <Modal open={this.props.open} closeOnDimmerClick>
                <Modal.Header>Settings</Modal.Header>
                <Modal.Content scrolling>

                    <Modal.Description>
                        <Form>
                            <FormInput type="number" label="Padding" value={this.state.padding} onChange={(e, { value }) => { this.setState({ padding: parseInt(value) || 0 }) }} />
                            <FormInput type="number" label="Max Width" value={this.state.width} onChange={(e, { value }) => { this.setState({ width: parseInt(value) || 0 }) }} />
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.props.close}>
                        Cancel
                    </Button>
                    <Button onClick={this.save.bind(this)} primary>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
    save() {
        this.props.close();
        localStorage.setItem("padding", this.state.padding.toString());
        localStorage.setItem("width", this.state.width.toString());
        this.props.onUpdated && this.props.onUpdated();
    }
}