import Color from './Color';

export default class Mat {
    static types = {
        Grayscale: 1,
        RGBA: 2
    }
    private _type: number;
    private _width: number;
    private _height: number;
    public _buffer: Uint8ClampedArray;
    public test;

    constructor(type: number);
    constructor(width: number, height: number);
    constructor(type: number, width: number, height: number);
    constructor(type: number, width: number, height: number, buffer: Uint8ClampedArray);
    constructor(x, y?, z?, b?) {
        switch (arguments.length) {
            case 1:
                this._type = x;
                break;
            case 2:
                this._width = x;
                this._height = y;
                this._buffer = new Uint8ClampedArray((x * y) * (this._type == 1 ? 1 : 4));
                this.init();
                break;
            case 3:
                this._type = x;
                this._width = y;
                this._height = z;
                this._buffer = new Uint8ClampedArray((y * z) * (this._type == 1 ? 1 : 4));
                this.init();
                break;
            case 4:
                this._type = x;
                this._width = y;
                this._height = z;
                this._buffer = b;
                break;
        }
    }

    get height() {
        return this._height;
    }
    get width() {
        return this._width;
    }
    get type() {
        return this._type;
    }
    get channels() {
        return this._type == 1 ? 1 : 4;
    }
    public clone() {
        return new Mat(this._type, this._width, this._height, this._buffer);
    }
    init() {
        for (var i = this.channels - 1; i < this._buffer.length; i += this.channels) {
            this._buffer[i] = 255;
        }
    }
    pixel(x: number, y: number): Color {
        switch (this._type) {
            case 1:
                var gray = this._buffer[(y * this._width) + x];
                return new Color(gray, gray, gray);
            case 2:
                var index = ((y * this._width) + x) * 4;
                return new Color(this._buffer[index], this._buffer[index + 1], this._buffer[index + 2], this._buffer[index + 3]);
        }
    }
    setPixel(x: number, y: number, color: Color): void {
        switch (this._type) {
            case 1:
                this._buffer[(y * this._width) + x] = (color.red + color.green + color.blue) / 3;
                break;
            case 2:
                var index = ((y * this._width) + x) * 4;
                this._buffer[index] = color.red;
                this._buffer[index + 1] = color.green;
                this._buffer[index + 2] = color.blue;
                break;
        }
    }
    fill(x: number, y: number, width: number, height: number, color: Color): void {
        for (var yy = y; yy < height + y; yy++) {
            for (var xx = x; xx < width + x; xx++) {
                this.setPixel(xx, yy, color);
            }
        }
    }

    toRGBA() {
        if (this._type == 1) {
            var newBuffer = new Uint8ClampedArray((this._width * this._height) * 4);
            for (var i = 0; i < this._buffer.length; i++) {
                let index = i * 4;
                newBuffer[index] = newBuffer[index + 1] = newBuffer[index + 2] = this._buffer[i];
                newBuffer[index + 3] = 255;
            }
            this._type = 2;
            this._buffer = newBuffer;
        }
    }

    toGrayscale() {
        if (this._type == 2) {
            var newBuffer = new Uint8ClampedArray(this._width * this._height);
            for (var i = 0; i < this._buffer.length; i += 4) {
                newBuffer[i / 4] = (this._buffer[i] + this._buffer[i + 1] + this._buffer[i + 2]) / 3;
            }
            this._type = 1;
            this._buffer = newBuffer;
        }
    }

    get imageData(): ImageData {
        this.toRGBA();
        let imageData = new ImageData(this._buffer, this._width, this._height);
        return imageData;
    }
}