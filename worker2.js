importScripts("opencv_js.js");

self.cv = cv();
self.addEventListener('message', function (e) {
    var Mat = cv.matFromArray(e.data.height, e.data.width, cv.CV_8UC4, e.data.data);
    cv.cvtColor(Mat, Mat, cv.COLOR_RGB2GRAY);
    var blurSize = 5;
    var threshold = 100;
    cv.blur(Mat, Mat, new cv.Size(blurSize, blurSize));
    cv.Canny(Mat, Mat, threshold, threshold * blurSize, blurSize, false);
    cv.blur(Mat, Mat, new cv.Size(blurSize, blurSize));
    let contours = new cv.MatVector();
    let hierarchy = new cv.Mat();

    cv.findContours(Mat, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
    var area = 0;
    var largestIndex = 0;
    for (let i = 0; i < contours.size(); ++i) {
        var a2 = cv.contourArea(contours.get(i), false);
        if (a2 > area) {
            area = a2;
            largestIndex = i;
        }
    }
    var rect = cv.boundingRect(contours.get(largestIndex));
    var padding = e.data.padding;
    var doublePadding = padding * 2;
    self.postMessage({ x: rect.x - padding, y: rect.y - padding, width: rect.width + doublePadding, height: rect.height + doublePadding, i: e.data.i });
});
// console.log(cv.Canny);