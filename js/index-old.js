// /**
//  * Basic Mat Class
//  * @constructor
//  * @param {number} width
//  * @param {number} height
//  */
// function Mat(width, height) {
//     this.width = width;
//     this.height = height;
//     this.buffer = new Uint8ClampedArray((height * width) * 4);
//     this.type = Mat.Types.RGB;
// }

// Mat.Types = {
//     Grayscale: 0,
//     RGB: 1
// };

// /**
//  * Draws A DOM image to the mat
//  */
// Mat.prototype.drawImage = function (image) {
//     var canvas = document.createElement('canvas');
//     canvas.width = image.width;
//     canvas.height = image.height;
//     var ctx = canvas.getContext('2d');
//     ctx.drawImage(image, 0, 0);
//     this.buffer = ctx.getImageData(0, 0, image.width, image.height).data;
// }

// Mat.prototype.fill = function (x, y, width, height, rgb) {
//     for (var i = 0; i < height; i++) {
//         for (var i2 = 0; i2 < width; i2++) {
//             var index = (((i + y) * this.width) + x + i2) * 4;
//             this.buffer[index] = rgb.red;
//             this.buffer[index + 1] = rgb.green;
//             this.buffer[index + 2] = rgb.blue;
//             this.buffer[index + 3] = 255;
//         }
//     }
// }

// Mat.prototype.slice = function (x, y, width, height) {
//     var array = [];
//     for (var i = 0; i < height; i++) {
//         for (var i2 = 0; i2 < width; i2++) {
//             var index = (((i + y) * this.width) + x + i2) * 4;
//             array.push(this.buffer[index]);
//             array.push(this.buffer[index + 1]);
//             array.push(this.buffer[index + 2]);
//             array.push(this.buffer[index + 3]);
//         }
//     }
//     return array;
// }

// Mat.prototype.eyeDropper = function (x, y) {
//     var index = ((y * this.width) + x) * 4;
//     return new RGB(this.buffer[index], this.buffer[index + 1], this.buffer[index + 2]);
// }

// function RGB(red, green, blue) {
//     this.red = red;
//     this.green = green;
//     this.blue = blue;
// }

// var avg = function (arr) {
//     var red = 0;
//     var green = 0;
//     var blue = 0;

//     for (var i = 0; i < arr.length; i += 4) {
//         red = red + arr[i];
//         green = green + arr[i + 1];
//         blue = blue + arr[i + 2];
//     }
//     var pixelCount = (arr.length / 4);
//     return new RGB(Math.round(red / pixelCount), Math.round(green / pixelCount), Math.round(blue / pixelCount));
// }

// var ImageProc = {
//     blur: function (mat, radius) {
//         var pixelLength = mat.type == Mat.Types.Grayscale ? 1 : 4;
//         for (var i = -radius; i < mat.height; i += radius) {
//             for (var i2 = -radius; i2 < mat.width; i2 += radius) {
//                 mat.fill(i2 + radius, i + radius, radius, radius, avg(mat.slice(i2 + radius, i + radius, radius, radius)));
//             }
//         }
//         return mat;
//     },
//     /**
//      * @param {Mat} mat
//      * @param {Number} radius
//      * @param {Number} threshold
//      * @returns {Mat}
//      */
//     findShape: function (mat, radius, threshold) {
//         var origY = radius;
//         var origX = radius;
//         var outline = [];

//         for (var y = origY; y < mat.height - (radius * 2); y += radius) {
//             for (var x = origX; x < mat.width - (radius * 2); x += radius) {
//                 /**
//                  * hab
//                  * gZc
//                  * fed
//                  */

//                 var Z = mat.eyeDropper(x, y);
//                 var a = mat.eyeDropper(x, y - radius);
//                 var b = mat.eyeDropper(x + radius, y - radius);
//                 var c = mat.eyeDropper(x + radius, y);
//                 var d = mat.eyeDropper(x + radius, y + radius);
//                 var e = mat.eyeDropper(x, y + radius);
//                 var f = mat.eyeDropper(x - radius, y + radius);
//                 var g = mat.eyeDropper(x - radius, y);
//                 var h = mat.eyeDropper(x - radius, y - radius);

//                 if (
//                     ImageProc.compareColor(Z, threshold, a, b, c, d, e, f, g, h)
//                 ) {
//                     outline.push([x, y]);
//                     mat.fill(x, y, radius, radius, new RGB(255, 0, 255));
//                 }
//             }
//         }
//         var firstX = mat.width, lastX = 0, firstY = mat.height, lastY = 0;
//         outline.forEach(function (chunk) {
//             if (chunk[0]) {
//                 if (chunk[0] < firstX) {
//                     firstX = chunk[0];
//                 }
//                 if (chunk[0] > lastX) {
//                     lastX = chunk[0];
//                 }
//             }
//             if (chunk[1]) {
//                 if (chunk[1] < firstY) {
//                     firstY = chunk[1];
//                 }
//                 if (chunk[1] > lastY) {
//                     lastY = chunk[1];
//                 }
//             }
//         });
//         return { mat: mat, x: firstX, y: firstY, width: lastX - firstX, height: lastY - firstY };
//     },
//     /**
//      * @param {Number} threshold
//      * @returns {boolean}
//      */
//     compareColor: function (Z, threshold) {
//         for (var i = 2; i < arguments.length; i++) {
//             if (Z.red - arguments[i].red >= threshold || arguments[i].red - Z.red >= threshold) {
//                 return true;
//             }
//             if (Z.green - arguments[i].green >= threshold || arguments[i].green - Z.green >= threshold) {
//                 return true;
//             }
//             if (Z.blue - arguments[i].blue >= threshold || arguments[i].blue - Z.blue >= threshold) {
//                 return true;
//             }
//         }
//         return false;
//     },
//     processImage: function (target) {
//         var mat = new Mat(target.width, target.height);
//         mat.drawImage(target);
//         var canvas = document.createElement('canvas');
//         var ctx = canvas.getContext('2d');
//         var radius = 40;
//         mat = ImageProc.blur(mat, radius);

//         var result = ImageProc.findShape(mat, radius, 100);
//         mat = result.mat;

//         var im = new ImageData(mat.buffer, mat.width, mat.height);
//         canvas.height = mat.height;
//         canvas.width = mat.width;
//         ctx.putImageData(im, 0, 0);
//         // ctx.drawImage(target, -result.x, -result.y);
//         return canvas.toDataURL("png");
//     }
// }

// $(document).on('click', '.crop-image', function (e) {
//     var img = $(this).closest('.card').find('img');
//     var id = img.data('id');
//     img.attr('src', ImageProc.processImage($('.src-images img[data-id="' + id + '"]').get(0)));
// });

// function readmultifiles(files) {
//     var reader = new FileReader();
//     function readFile(i) {
//         if (i >= files.length) return;
//         var file = files[i];
//         reader.onload = function (e) {
//             // get file content  
//             var data = e.target.result;
//             var img = $('<img class="src-image" data-id="' + (++index) + '"/>');
//             img.attr('src', data);
//             $('.src-images').append(img);
//             $('.preview-images').append(cardTemplate(data));
//             // do sth with bin
//             readFile(i + 1)
//         }
//         reader.readAsDataURL(file);
//     }
//     readFile(0);
// }

// var index = 0;

// var cardTemplate = function (data) {
//     return `
//     <div class="column">
//         <div class="ui inverted card">
//             <div class="image">
//                 <img src="${data}" data-id="${index}" />
//             </div>
//             <div class="ui bottom attached secondary button crop-image">
//                 Crop
//             </div>
//         </div>
//     </div>
//     `;
// }

// $(function () {
//     $('.main-sidebar,.main-content').height($('body').height());
//     $('body')
//         .on('drop', function (ev) {
//             ev.preventDefault();
//             $('.ui.basic.modal').modal('hide');
//             var files = ev.originalEvent.dataTransfer.files;
//             readmultifiles(files, index);
//         })
//         .on('dragover', function (ev) {
//             ev.preventDefault();
//             if (!$('.ui.basic.modal').modal('is active')) {
//                 $('.ui.basic.modal').modal('show');
//             }
//         })
//         .on('dragleave', function () {
//             // $('.ui.basic.modal').modal('hide');
//         });
// });

var blur = function (canvas, depth) {
    var scale = 1;
    var ctx = canvas.getContext('2d');
    for (var i = 0, w, h; i < depth; i++) {
        w = canvas.width / scale;
        h = canvas.height / scale;
        scale *= 1.25;
        ctx.drawImage(canvas, 0, 0, w, h,
            0, 0, canvas.width / scale, canvas.height / scale);

    }

    /// up sample
    for (var i = 0, w, h; i < depth; i++) {
        w = canvas.width / scale;
        h = canvas.height / scale;
        scale /= 1.25;
        ctx.drawImage(canvas, 0, 0, w, h,
            0, 0, canvas.width / scale, canvas.height / scale);

    }
}

var xy2index = function (x, y, width) {
    return ((y * width) + x) * 4;
}

var edgeDetect = function (canvas) {
    var ctx = canvas.getContext('2d');
    var data = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
    for (var y = 0; y < canvas.height; y++) {
        for (var x = 0; x < canvas.width; x++) {
            data[xy2index(x, y, canvas.width)];
        }
    }
}

var img = document.createElement('img');
img.addEventListener('load', function () {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    c.width = img.width;
    c.height = img.height;
    ctx.drawImage(img, 0, 0);
    var data = ctx.getImageData(0, 0, img.width, img.height).data;
    var data2 = new Uint8ClampedArray(data.length);
    for (var i = 0; i < data.length; i += 4) {
        var gray = (data[i] + data[i + 1] + data[i + 2]) / 3;
        data2[i] = data2[i + 1] = data2[i + 2] = gray;
        data2[i + 3] = 255;
    }
    var im = new ImageData(data2, img.width, img.height);
    ctx.putImageData(im, 0, 0);
    blur(c, 10);
    edgeDetect(c);
    document.body.appendChild(c);
});
img.src = "IMG_3872.JPG";
document.body.appendChild(img);